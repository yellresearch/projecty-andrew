#!/bin/bash
# Fill in DBNAME with the database name. Default value: alfresco
# Fill in CONTAINERNAME with whatever you want to name the container for ease of identification. Default value: alfcont
# If you add the parameter "build" to the script call, it will build the necessary content-store folders and image
DBNAME=ing_wcm
CONTAINERNAME=ing_wcm
DOCKER_FOLDER=/data/software/releases/ING_WCM
STORAGE_HOST=
STORAGE_USERNAME=
STORAGE_FOLDER=/home

if [ "$1" == "build" ]; then
        if [ "$2" != "nopull" ]; then
                rsync -av --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/startup.sh .
                rsync -av --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/support-files .
                rsync -av --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/docker-run.sh .
                rsync -av --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/alfresco-global.properties .
                rsync -av --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/Dockerfile .

                rsync -av --delete --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/alfresco .
                rsync -av --delete --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/share .

                dos2unix startup.sh
                dos2unix docker-run.sh
                dos2unix alfresco-global.properties
                dos2unix Dockerfile
        fi
else
        if [ "$1" != "nopull" ]; then
                rsync -av --delete --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/alfresco .
                rsync -av --delete --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/share .
        fi
fi

if [ "$1" == "build" ]; then
        mkdir alf_data
        mkdir alf_data/$DBNAME
        mkdir tomcat/webapps/alfresco
        mkdir tomcat/webapps/share
        mkdir tomcat/shared
        mkdir tomcat/shared/classes
        mkdir tomcat/shared/classes/alfresco
        chmod -R 777 alf_data
        sed -i -- 's/shared.loader=/shared.loader=${catalina.base}\/shared\/classes,${catalina.base}\/shared\/lib\/*.jar/g' tomcat/conf/catalina.properties
        echo "export JAVA_OPTS='-server -XX:MaxPermSize=256M -Xmx2G -Xms2G -Xss1024k'" > tomcat/bin/setenv.sh
        cp $DOCKER_FOLDER/support-files/mysql-* tomcat/lib
        cp alfresco-global.properties tomcat/shared/classes
        sudo docker build -t yellresearch/server:alfresco .
else
        docker rm -f $CONTAINERNAME
        docker run -d -p 192.168.99.2:8009:8009 -e "dbname=$DBNAME" --name=$CONTAINERNAME -v $DOCKER_FOLDER/alf_data/$DBNAME:/opt/alf_data/$DBNAME -v $DOCKER_FOLDER/webapps:/opt/tomcat/webapps -v $DOCKER_FOLDER/support-files:/opt/tomcat/shared/classes/alfresco -v $DOCKER_FOLDER/tomcat:/opt/tomcat -v $DOCKER_FOLDER/alfresco-enterprise-5.0.1/alf_data/keystore:/opt/alf_data/keystore  -v $DOCKER_FOLDER/alfresco-enterprise-5.0.1/solr4:/opt/solr4 -v $DOCKER_FOLDER/alf_data/solr4:/opt/alf_data/solr4 yellresearch/server:alfresco
fi
