FROM ubuntu:14.04
MAINTAINER Tan Keh Win <kehwin@yellresearch.com>

ENV DBNAME alfresco
ENV JAVA_PACKAGE jdk-7u67-linux-x64.tar.gz
ENV JAVA_EX_FOLDER jdk1.7.0_67
ENV JAVA_BASE /usr/lib/jvm/java-oracle
ENV JAVA_HOME ${JAVA_BASE}/${JAVA_EX_FOLDER}
ENV CATALINA_HOME /tomcat

ADD ${JAVA_PACKAGE} ${JAVA_BASE}
ADD startup.sh /

RUN mkdir tomcat && \
        mkdir opt/alf_data && \
        mkdir opt/alf_data/${DBNAME}

RUN for binary in `ls ${JAVA_HOME}/bin`; do \
      if [ ! -z "${binary##*.*}" ] && [ ! -f /usr/bin/$binary ]; then \
        update-alternatives --install \
          /usr/bin/$binary $binary ${JAVA_HOME}/bin/$binary 100; \
      fi \
    done

RUN chmod a+rwx /*.sh

EXPOSE 8080

CMD ["/startup.sh"]
